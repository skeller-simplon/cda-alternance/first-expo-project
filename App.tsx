import React from "react";
import { SafeAreaView } from "react-native";
import "react-native-gesture-handler";
import MainNavigator from "./src/navigation";

export default function App() {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <MainNavigator />
    </SafeAreaView>
  );
}
