import React, { useState } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import AddTodo from "./todoList/components/AddTodo";
import ToDoList from "./todoList/components/ToDoList";
import { ListItem } from "./todoList/types/ListItem";
import { Status } from "./todoList/types/Status";

const LIST_KEY = "itemList";
let nextId = 0;

export default function ToDoListMain() {
  const fetchLocalStorageItems = (): ListItem[] => {
    if (Platform.OS === "web" || Platform.OS === "windows")
      return JSON.parse(localStorage.getItem(LIST_KEY) || "[]");
    else return [];
  };

  const [listItems, setListItems] = useState<Array<ListItem>>(
    fetchLocalStorageItems()
  );

  const setListItemsWithLocalStorage = (items: Array<ListItem>) => {
    if (Platform.OS === "web" || Platform.OS === "windows")
      localStorage.setItem(LIST_KEY, JSON.stringify(items));
    setListItems(items);
  };

  const onAddTodoInput = (todo: ListItem) => {
    let items = [...listItems, { ...todo, id: nextId++ }];
    setListItemsWithLocalStorage(items);
  };

  const onDoneButton = (todo: ListItem) => {
    setListItemsWithLocalStorage(
      listItems.map((i) => {
        if (i.id === todo.id) i.status = Status.CLOSED;
        return i;
      })
    );
  };

  return (
    <View style={styles.container}>
      <ToDoList listItems={listItems} onDonePress={onDoneButton} />
      <AddTodo onAddTodoAction={onAddTodoInput} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "grey",
    justifyContent: "center",
  },
});
