interface IQuote{
    _id: string,
    created_at: string,
    favorite_count: number,
    _id_str: string,
    in_reply_to_user_id_str: string,
    is_retweet: boolean,
    retweet_count: number,
    source: string,
    text: string
}