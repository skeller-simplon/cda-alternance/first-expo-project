import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from "react";

import QuotesScreen from "../components/quotes/QuotesScreen";
import TasksScreen from "../components/tasks/TasksScreen";
import { Routes } from "./constants";
import { FontAwesome } from "@expo/vector-icons";
import CameraScreen from "../camera/CameraScreen";

// on type notre stack en lui indiquant nos routes et les paramètres qu'elles peuvent prendre
// ici nos 2 routes n'ont pas de params donc on met undefined
export type MainStackParamList = {
  [Routes.QUOTES]: undefined;
  [Routes.TASKS]: undefined;
  [Routes.CAMERA]: undefined;
};

/**
 * Si nos routes avaient des params on aurait pu typer de cette façon :
 * type MainStackParamList = {
 *  [Routes.QUOTES]: {
 *    data: ...someData // dans mes params j'aurais une clef data
 *    };
 * }
l'intérêt de ça, c'est de typer le payload de nos params
 */

// ici j'ai "renommé" la clé Navigator retournée par createBottomTabNavigator() en Stack grâce à la destructuration
const { Navigator: Stack, Screen } =
  createBottomTabNavigator<MainStackParamList>(); // on utilise notre type de cette façon

const MainStack = () => {
  // je donne explicitement l'info à ma stack que je veux QUOTES comme écran par défaut de la stack
  // si je ne renseigne pas cette props, alors le premier écran de la stack sera l'écran par défaut de la stack

  // je customise mes screens avec la props options, qui est un objet avec des clés tabBarIcon, tabBarLabel, ...
  return (
    <Stack initialRouteName={Routes.QUOTES}>
      <Screen
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome
              name="quote-right"
              size={24}
              color={focused ? "blue" : "black"}
            />
          ),
          tabBarLabel: "Citations",
        }}
        name={Routes.QUOTES}
        component={QuotesScreen}
      />
      <Screen
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome
              name="check"
              size={24}
              color={focused ? "blue" : "black"}
            />
          ),
          tabBarLabel: "Tâches",
        }}
        name={Routes.TASKS}
        component={TasksScreen}
      />
      <Screen
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome
              name="camera"
              size={24}
              color={focused ? "blue" : "black"}
            />
          ),
        }}
        name={Routes.CAMERA}
        component={CameraScreen}
      />
    </Stack>
  );
};

export default MainStack;
