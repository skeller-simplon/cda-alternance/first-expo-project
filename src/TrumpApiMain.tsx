import React, { useEffect, useState } from "react";
import { RefreshControl, Button, FlatList, StyleSheet, Text, View, ScrollView } from "react-native";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

const STORAGE_KEY = "@storage_Key";

export default function TrumpApiMain() {
  const [quotes, setQuotes] = useState<IQuote[]>([]);
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    fetchQuote()
    const wait = (timeout: number) => {
      return new Promise(resolve => setTimeout(resolve, timeout));
    }
    wait(2000).then(() => setRefreshing(false));
  }, []);

  const storeData = async (value: IQuote[]) => {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(value));
      fetchData()
    } catch (e) {
      console.error(e);
    }
  };

  const fetchData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem(STORAGE_KEY);
      const parsedValue = jsonValue != null ? JSON.parse(jsonValue) : [];
       setQuotes(parsedValue)
       return parsedValue
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    fetchData()
  }, )

  const fetchQuote = () => {
    axios
      .get("http://www.trumptweets.rest/api")
      .then((res) => {
        storeData([...quotes, res.data])
      })
      .catch((err) => {
        console.error(err);
      });
  };

  useEffect(() => {
    fetchQuote();
  }, []);

  return (
      <FlatList
        ListHeaderComponent={<Button onPress={fetchQuote} title="Add a new quote" />}
        // Ne marche po
        ListFooterComponent={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
        data={quotes}
        keyExtractor={(quote) => quote._id}
        renderItem={(quote) => (
          <Text style={{ ...styles.blockText }}>{quote.item.text}</Text>
        )}
      />
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: "center",
  },
  blockText: {
    margin: 10,
  },
});
