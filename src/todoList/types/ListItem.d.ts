import { CATEGORY_TYPE } from "./Categories";

type ListItem = {
    id: number;
    title: string;
    description?: string;
    status: string;
    catégorie: CATEGORY_TYPE;
}