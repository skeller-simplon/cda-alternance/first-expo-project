const CATEGORIES = ["TRAVAIL", "LOISIR"]
type CATEGORY_TYPE = typeof CATEGORIES[number] | null;

export {CATEGORIES, CATEGORY_TYPE}