import React, { useState } from 'react';
import { FlatList, StyleSheet, View, TextInput, Button, Alert } from 'react-native';
import { CATEGORIES, CATEGORY_TYPE } from '../types/Categories';
import { Status } from '../types/Status';
import RNPickerSelect from "react-native-picker-select"
import { ListItem } from '../types/ListItem';

type addTodoProps = {
    onAddTodoAction: (listItem: ListItem) => void
}

export default function addTodo({ onAddTodoAction }: addTodoProps) {
    const [inputValue, setInputValue] = useState<ListItem>(
        {
            title: "",
            catégorie: null,
            description: "",
            id: 0,
            status: Status.OPEN
        }
    );

    const checkMandatoryFields = () => {
        console.log(inputValue);
        
        if (!inputValue.catégorie) {
            Alert.alert("Catégorie nécessaire");
            return;
        }
        if (!inputValue.title) {
            Alert.alert("Titre nécessaire");
            return;
        }

    }
    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                onChangeText={(desc) => setInputValue({ ...inputValue, title: desc })}
                value={inputValue.title}
                placeholder="Titre"
                keyboardType="default"
            />
            <TextInput
                style={styles.input}
                onChangeText={(desc) => setInputValue({ ...inputValue, description: desc })}
                value={inputValue.description}
                placeholder="Description"
                keyboardType="default"
            />
            <RNPickerSelect
                onValueChange={(value: string) => setInputValue({ ...inputValue, catégorie: value as CATEGORY_TYPE })}
                items={CATEGORIES.map(m => { return {label: m, value: m }})}
            />
            <Button
                onPress={() => onAddTodoAction(inputValue)}
                title="Submit"
                color="#841584"
                accessibilityLabel="Create new todo"
            />

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flexDirection: "row",
        marginHorizontal: 16,
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        margin: "5px",
        backgroundColor: "white"
    }
});
