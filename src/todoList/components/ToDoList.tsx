import React, { useState } from 'react';
import { FlatList, StyleSheet, View, Text, Button } from 'react-native';
import { ListItem } from '../types/ListItem';
import { Status } from '../types/Status';

interface ToDoListProps {
  listItems: Array<ListItem>,
  onDonePress: (item: ListItem) => void
}


export default function TodoList({ listItems, onDonePress }: ToDoListProps) {

  const [itemsCategories] = useState([...new Set(listItems.map(i => i.catégorie))]);


  return (
    <View style={styles.container}>
      {itemsCategories && itemsCategories.map(category => {
        return (
          <>
            <Text style={styles.title} >{category}</Text>
            <FlatList
              data={listItems.filter(item => item.catégorie === category && item.status === Status.OPEN)}
              keyExtractor={(item, i) => category + "-" + item.id + "-" + i}
              renderItem={({ item }) => {
                return (
                  <View style={styles.item}>
                    < View style={{flexDirection: 'column'}}>
                      <Text style={styles.itemTitle}>{item.title}</Text>
                      <Text style={styles.itemDescription}>{item.description}</Text>
                    </View>
                    <Button key={"btn-" + category + "-" + item.id} title="C'est fait" onPress={() => onDonePress(item)} />
                  </View>
                )
              }}
            />
          </>
        )
      })}

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column"
  },
  title: {
    fontSize: 30,
    paddingLeft: 10,
    color: "white"
  },
  item: {
    margin: 20,
    padding: 10,
    backgroundColor: "white",
    flexDirection: 'row',
    alignContent: "space-between"
  },
  itemTitle: {
    fontSize: 20,
    textDecorationLine: "underline"
  },
  itemDescription: {
    marginLeft: "30px"
  }
});
