import React from "react";
import { FlatList, RefreshControl, StyleSheet, Text, View } from "react-native";
import QuoteItem from "./QuoteItem";

type Props = {
  quotes: IQuote[];
  isLoading: boolean; // représente notre état de chargement
  onRefresh: () => void; // va exécuter une fonction dans le parent (évènement)
};

const QuotesList = ({ quotes, isLoading, onRefresh }: Props) => {
  // la props refreshControl nous permet de gérer l'état de chargement de notre liste
  // ça va nous permettre de faire une action, ici charger une nouvelle quote, lorsqu'on swipe vers le bas
  //
  // si je n'ai pas de quote (tableau vide) alors je n'affiche pas la liste mais un texte
  return (
    <View style={styles.container}>
      {quotes.length ? (
        <FlatList
          refreshControl={
            <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
          }
          data={quotes}
          renderItem={({ item }) => <QuoteItem quote={item} />}
          keyExtractor={({ message }, index) => message + index}
        />
      ) : (
        <Text>Pas de citation stockée pour l'instant...</Text>
      )}
    </View>
  );
};

export default QuotesList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
