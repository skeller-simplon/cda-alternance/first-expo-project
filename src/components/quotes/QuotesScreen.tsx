import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect, useState } from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import { Routes } from "../../navigation/constants";
import { MainStackParamList } from "../../navigation/MainStack";
import { getRandomQuote } from "../../services/quotes";
import Layout from "../shared/Layout";
import QuotesList from "./QuotesList";

// je type ici ma props navigation que je récupère de <Screen /> et qui est passée à ce composant qui représente mon screen
type QuotesScreenNavigationProp = StackNavigationProp<
  // j'utilise le typage qui vient de react-navigation qui prend "2 arguments de typage"
  MainStackParamList, // le premier c'est le typage de la stack dans laquelle je suis
  Routes.QUOTES // puis le nom du screen sur lequel je suis
>;

// je type les props de mon composant en ajoutant la props navigation avec le typage que je viens de créer au dessus
type Props = {
  navigation: QuotesScreenNavigationProp;
};

// je peux utiliser la props navigation bien typée maintenant et ai accès aux propriétés pour naviguer
const QuotesScreen = ({ navigation }: Props) => {
  const [quotes, setQuotes] = useState<IQuote[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const { navigate } = useNavigation(); // ce hook me permet d'accéder aux propriétés de navigation sans avoir à utiliser la props (et donc plus besoin de typer les props)

  // useEffect nous permet d'exécuter du code si la référence
  //   useEffect(() => {
  //     const fetchQuote = async () => {
  //       const quote = await getRandomQuote(); // store the result of our promise
  //
  //       setQuote(quote); // set the fetched quote to the local state (quotes)
  //     };
  //
  //     fetchQuote();
  //   }, []); <<<< array de dépendances vide = ne vas s'exécuter qu'une fois (au premier render du composant)

  const fetchQuote = async () => {
    try {
      // avant de faire le call API j'active mon chargement (state)
      setIsLoading(true);

      // je fetche une citation random via mon service
      const fetchedQuote = await getRandomQuote();

      // je stocke ma citation dans le storage en créant un nouvel array
      await AsyncStorage.setItem(
        "@quotes",
        JSON.stringify([...quotes, fetchedQuote])
      );

      // une fois que tous mes traitements sont finis je reset mon loading à false
      setIsLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  const getStoredQuotes = async () => {
    try {
      // on get nos données depuis le storage
      const storedQuotes = await AsyncStorage.getItem("@quotes");

      // getItem peut renvoyer null si il trouve pas la clé qu'on demande
      if (storedQuotes === null) {
        // si la clé est introuvable on créée un tableau vide
        const emptyQuotesArray: IQuote[] = [];

        // ... on set le tableau vide dans le storage
        await AsyncStorage.setItem("@quotes", JSON.stringify(emptyQuotesArray));
        // ... et on set le tableau vide dans le state (pour qu'ils aient la même valeur)
        setQuotes(emptyQuotesArray);
      } else {
        // on parse le JSON retourné par getItem
        const parsedStoredQuotes = JSON.parse(storedQuotes);

        // on set nos quotes du storage dans notre state
        // notre state est maintenant synchronisé avec notre storage
        setQuotes(parsedStoredQuotes);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getStoredQuotes(); // à chaque fois que la référence de notre state "quotes" change, on re-get les quotes de notre store et on met à jour notre state
  }, [quotes]); // quand la référence de quotes change, ça va réexécuter le useEffect. Si je mets un array vide, le useEffect ne s'exécutera qu'au premier render (pas de dépendance = pas de réexécution)

  // j'utilise .reverse() pour avoir la quote fetchée en dernier en haut (UX)
  // j'utilise navigate() qui vient du hook useNavigation pour passer d'un écran à l'autre
  return (
    <Layout title="Donald a dit">
      <Text style={styles.subtitle}>
        {quotes.length} {quotes.length > 1 ? "citations" : "citation"}
      </Text>
      <QuotesList
        quotes={quotes.reverse()}
        isLoading={isLoading}
        onRefresh={fetchQuote}
      />
      <Button onPress={fetchQuote} title="Charger une citation" />
      <Button onPress={() => AsyncStorage.clear()} title="Supprimer tout" />
      <View style={styles.divider} />
      <Button title="Aller aux tâches" onPress={() => navigate("Tasks")} />
    </Layout>
  );
};

const styles = StyleSheet.create({
  subtitle: {
    textAlign: "right",
    marginBottom: 10,
    marginTop: -20,
  },
  divider: {
    borderBottomColor: "#ddd",
    borderBottomWidth: 1,
    opacity: 0.5,
    paddingVertical: 10,
  },
});

export default QuotesScreen;
