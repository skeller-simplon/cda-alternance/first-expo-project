import React from "react";
import { StyleSheet, Text, View } from "react-native";

type Props = {
  quote: IQuote;
};

const QuoteItem = ({ quote }: Props) => {
  return (
    <View style={styles.container}>
      <Text>{quote.message}</Text>
    </View>
  );
};

export default QuoteItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#F1F1F1",
    padding: 10,
    borderRadius: 10,
    marginBottom: 5,
  },
});
