import React, { useState } from "react";
import { FlatList } from "react-native";
import { Task, TaskStatus } from "../../types/Task";
import Layout from "../shared/Layout";
import AddTaskForm from "./AddTaskForm";
import TaskItem from "./TaskItem";

// fausses données pour l'exemple
const DATA: Task[] = [
  {
    id: 1,
    status: TaskStatus.TODO,
    title: "Faire la vaisselle",
  },
  {
    id: 2,
    status: TaskStatus.TODO,
    title: "Faire la lessive",
  },
  {
    id: 3,
    status: TaskStatus.TODO,
    title: "Faire à manger",
  },
];

const TasksScreen = () => {
  const [data, setData] = useState<Task[]>(DATA);

  const onTaskDone = (id: number) => {
    // avec filter je créée un nouveau tableau en enlevant la task qui a l'id en paramètre de onTaskDone
    const updatedData = data.filter((task) => task.id !== id);

    // je mets à jour mon state
    setData(updatedData);
  };

  // dans la props onAdd on set nos nouvelles données dans le state (avec setData).
  // on lui donne une copie de notre state existant (grâce au spread operator) en lui ajoutant notre nouvelle task.
  return (
    <Layout title="Mes tâches">
      <FlatList
        data={data.filter((task) => task.status === TaskStatus.TODO)}
        renderItem={({ item }) => <TaskItem task={item} onDone={onTaskDone} />}
        keyExtractor={({ id }) => id.toString()}
      />
      <AddTaskForm
        label="Ajouter une tâche"
        onAdd={(taskTitle) =>
          setData([
            ...data,
            {
              id: data.length + 1,
              status: TaskStatus.TODO,
              title: taskTitle,
            },
          ])
        }
      />
    </Layout>
  );
};

export default TasksScreen;
