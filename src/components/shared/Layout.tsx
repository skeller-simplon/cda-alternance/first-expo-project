import React, { FC } from "react";
import { StyleSheet, Text, View } from "react-native";

type Props = {
  title: string;
};

const Layout: FC<Props> = ({ children, title }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {children}
    </View>
  );
};

export default Layout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    padding: 20,
    backgroundColor: "#fff",
  },
  title: {
    fontWeight: "900",
    fontSize: 32,
    marginBottom: 16,
  },
});
